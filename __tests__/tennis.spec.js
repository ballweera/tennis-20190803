// TennisApp
// - getScore(player) // A, B
// - echo() // return score sentense
// Example:
// 0 - 0    : "LOVE - LOVE"
// 15-0     : "Fifteen - LOVE"

// Test-Driven Development (TDD)

class TennisApp {
    constructor() {
        this.scoreA = 0
    }

    echo() {
        if (this.scoreA === 15) {
            return 'Fifteen - LOVE'
        } else if (this.scoreA === 30) {
            return 'Thirty - LOVE'
        }

        return 'LOVE - LOVE'
    }

    getScore(player) {
        if (this.scoreA === 0) {
            this.scoreA = 15
        } else if (this.scoreA === 15) {
            this.scoreA = 30
        }
    }
}

describe('TennisApp', () => {
    it('should return "LOVE - LOVE" when call echo() at game start', () => {
        // Arrange
        const app = new TennisApp()

        // Act
        let result = app.echo()

        // Assert
        expect(result).toBe('LOVE - LOVE')
    })

    it('should echo "Fifteen - LOVE" when playerA get first score', () => {
        // Arrange
        const app = new TennisApp()
        app.getScore('A')

        // Act
        let result = app.echo()

        // Assert
        expect(result).toBe('Fifteen - LOVE')
    })

    it('should echo "Thirty - LOVE" when playerA get double score', () => {
        // Arrange
        const app = new TennisApp()
        app.getScore('A')
        app.getScore('A')
        
        // Act
        let result = app.echo()

        // Assert
        expect(result).toBe('Thirty - LOVE')
    })
})